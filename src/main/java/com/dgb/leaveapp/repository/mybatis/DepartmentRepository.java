package com.dgb.leaveapp.repository.mybatis;

import com.dgb.leaveapp.model.Department;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import java.util.List;

/*

getAllDepartment
insertDepartment
updateDepartment
getDepartment

*/

@Repository
public interface DepartmentRepository {

    @Select("Select * from m_department")
    @Results(
            id = "departmentResult",value = {
            @Result(property = "descriptionEng",column = "description_en"),
            @Result(property = "createdAt",column = "created_at"),
            @Result(property = "createdBy",column = "created_by"),
            @Result(property = "modifyAt",column = "modified_at"),
            @Result(property = "modifyBy",column = "modified_by")
    }
    )
    List<Department> getAllDepartment();

    @Insert("insert into m_department( code, description, description_en, created_at, created_by, modified_at, modified_by\n" +
            ") values (#{code},#{description},#{descriptionEng},current_date,'admin',current_date,'admin')")
    boolean insertDepartment(Department department);

    @Update("Update m_department set code = #{code} ," +
            " description = #{description} , description_en = #{descriptionEng}" +
            ", modified_at = #{modifyAt} , modified_by = #{modifyBy} where id = #{id}")
    boolean updateDepartment(Department department);

    @Select("select * from m_department where id = #{id}")
    @ResultMap("departmentResult")
    Department getDepartment(int id);




    
}
