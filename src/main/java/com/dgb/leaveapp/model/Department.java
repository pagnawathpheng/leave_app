package com.dgb.leaveapp.model;

import java.io.Serializable;
import java.sql.Date;

public class Department implements Serializable {

    private int id;
    private String code;
    private String description;
    private String descriptionEng;
    private Date createdAt;
    private String createdBy;
    private Date modifyAt;
    private String modifyBy;

    public Department() {
    }

    public Department(int id, String code, String description, String descriptionEng, Date createdAt, String createdBy, Date modifyAt, String modifyBy) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.descriptionEng = descriptionEng;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifyAt = modifyAt;
        this.modifyBy = modifyBy;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", descriptionEng='" + descriptionEng + '\'' +
                ", createdAt=" + createdAt +
                ", createBy='" + createdBy + '\'' +
                ", modifyAt=" + modifyAt +
                ", modifyBy='" + modifyBy + '\'' +
                "}/n";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionEng() {
        return descriptionEng;
    }

    public void setDescriptionEng(String descriptionEng) {
        this.descriptionEng = descriptionEng;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createBy) {
        this.createdBy = createBy;
    }

    public Date getModifyAt() {
        return modifyAt;
    }

    public void setModifyAt(Date modifyAt) {
        this.modifyAt = modifyAt;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }
}
