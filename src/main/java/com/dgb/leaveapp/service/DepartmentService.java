package com.dgb.leaveapp.service;

import com.dgb.leaveapp.model.Department;

import java.util.List;

public interface DepartmentService {

    List<Department> getAllDepartment();

    Department addDepartment(Department inputDepartment);

    boolean editDepartment(Department inputDepartment);

    Department findDepartmentById(int id);

}
