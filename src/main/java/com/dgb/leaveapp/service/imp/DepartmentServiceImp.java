package com.dgb.leaveapp.service.imp;

import com.dgb.leaveapp.model.Department;
import com.dgb.leaveapp.repository.mybatis.DepartmentRepository;
import com.dgb.leaveapp.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImp implements DepartmentService {

    DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentServiceImp(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public List<Department> getAllDepartment() {
        return departmentRepository.getAllDepartment();
    }

    @Override
    public Department addDepartment(Department inputDepartment) {
        if(departmentRepository.insertDepartment(inputDepartment))
            return inputDepartment;
        return null;
    }

    @Override
    public boolean editDepartment(Department inputDepartment) {
        return departmentRepository.updateDepartment(inputDepartment);
    }

    @Override
    public Department findDepartmentById(int id) {
        return departmentRepository.getDepartment(id);
    }
}
