package com.dgb.leaveapp;

import com.dgb.leaveapp.model.Department;
import com.dgb.leaveapp.repository.mybatis.DepartmentRepository;
import com.dgb.leaveapp.service.DepartmentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;

@SpringBootTest
class LeaveAppApplicationTests {


    @Autowired
    DepartmentService departmentService;

    @Test
    void contextLoads() {

        System.out.println(departmentService.getAllDepartment());

        System.out.println(departmentService.findDepartmentById(2));

    }

}
